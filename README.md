# How to Create PSD2 X.509  Certificate Signing Request (CSR)

I will explain how to create Certificate Signing Request (CSR) according Open Banking Public Key Infrastructure (PKI).

***Before we start generating a certificate it is very important that you execute the commands one after the other.***

So here's what we're gonna do
## Steps

 1.  Install OpenSSL.
 2.  Create folder and download configuration files for certificate generation.
 3.  Customize OpenSSL Configuration
 4.  Create Certificate Signing Request (CSR) For  PSD2 QWAC X.509  certificate and generate private key
 5.  Create Certificate Signing Request (CSR) For  PSD2 QSEAL X.509  certificate and generate private key
 


##  Install OpenSSL version
In order to do all of these things, we need to have OpenSSL installed
Download and install OpenSSL V1.1.1 from :
https://slproweb.com/products/Win32OpenSSL.html

After the installation press Windows+R to open “Run” box. Type “cmd” and then click  Ctrl+Shift+Enter to open an administrator Command Prompt.

To check which OpenSSL version is installed on windows machin, run command :

    openssl version
    
In order to avoid problems during the certificate generation OpenSSl version should to be v 1.1.1  . When we are sure that we have the correct version of OpenSSl. We are ready to create folders and download configuration files for Generating of CSR and Private Key


## Create folder and download configuration files for certificate generation.

start creating the folder in Command Prompt to run  following command :

Create folder for private keys 

    mkdir private

Create folder for configuration files

	mkdir conf

download configuration files in  conf directory by web browser 

PSD2_QSEAL_config.cnf
PSD2_QWAC_config.cnf

I will explain in "Customize OpennSSL Configuration" section how to customize OpenSSL configuration files. According to the requirements of your TTP or ASPSP.

## Customize OpennSSL Configuration

The PSD2_QSEAL_config.cnf and PSD2_QWAC_config.conf configuration file is used to generate a CSR for an Open Banking certificate for digital signature and authentication using OpenSSL. The following fields  must be changed by the client to specific data pertinent to them.

PSD2_QSEAL_config.cnf

 - countryName = "GE"
 - organizationName = "Name of provider in English"
 - commonName = "Name of provider in English"
 - organizationIdentifier = "PSDGE-NBG-Provaider_code"



PSD2_QWAC_config.cnf

 - countryName = "GE"
 - organizationName = "Name of provider in English"
 - commonName = "Name of provider in English"
 - organizationIdentifier = "PSDGE-NBG-Provaider_code"


 - subjectAltName = DNS.1:sandbox1.testbank.ge, DNS.2:sandbox2.testbank.ge

## Create Certificate Signing Request (CSR) For  PSD2 QSEAL X.509  certificate and generate private key

openssl req -out aspsp_name_QSEAL.csr -new -newkey rsa:2048 -keyout private\aspsp_name_QSEAL_PrivateKey.key -config conf\PSD2_QSEAL_config.cnf



##  Create Certificate Signing Request (CSR) For  PSD2 QWAC X.509  certificate and generate private key


openssl req -out aspsp_name_QWAC.csr -new -newkey rsa:2048 -keyout private\aspsp_name_QWAC_PrivateKey.key -config conf\PSD2_QWAC_config.cnf




